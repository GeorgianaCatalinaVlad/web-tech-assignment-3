
const FIRST_NAME = "Vlad";
const LAST_NAME = "Georgiana-Catalina";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
class Employee {

    constructor(name,surname,salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

  
   
    // // //setteri si getteri
    // set name(value){
    //     this._name=value;
    // }

    // get name(){
    //     return this._name;
    // }

    // set surname(value){
    //     this._surname=value;
    // }

    // get surname(){
    //     return this._surname;
    // }
    
    // set salary(value){
    //     this._salary=value;
    // }

    // get salary(){
    //     return this._salary;
    // }
    
   getDetails() {
      return this.name+" "+this.surname+" "+this.salary;
  }
}


class SoftwareEngineer extends Employee {

   constructor(name,surname,salary,experience="JUNIOR"){
       super(name,surname,salary);
       this.experience=experience;

   }


// JUNIOR will get a 10% bonus from their salary;
// MIDDLE will get a 15% bonus from their salary;
// SENIOR will get a 20% bonus from their salary;
// If the employee has a different position than the ones mentioned above the bonus will be 10%;

   applyBonus(){
     if(this.experience==="JUNIOR"){
         return this.salary*1.10;
     }else 
     if(this.experience==="MIDDLE"){
         return this.salary*1.15;
     }else 
     if(this.experience==="SENIOR"){
         return this.salary*1.20;
     }else
     {
         return this.salary*1.10;
     }
   }
}




module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

